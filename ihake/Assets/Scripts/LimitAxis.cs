﻿using UnityEngine;
using System.Collections;

public class LimitAxis : MonoBehaviour {

    public GameObject leadCube;
	
	void Update () {

        if(transform.position.y < leadCube.transform.position.y)
        {
            transform.position = new Vector3(11f, leadCube.transform.position.y, 0f);
        }
	
	}
}
