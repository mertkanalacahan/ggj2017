﻿using UnityEngine;
using EZ_Pooling;

public class ObjectMove : MonoBehaviour
{

    public float speed = 0.1f;

    void OnSpawned()
    {
        speed = 0.1f + 0.1f * ((Time.time - GameManager.instance.gameStartingTime) / 30f);
    }

    void FixedUpdate()
    {
        transform.position = new Vector3(transform.position.x - speed, transform.position.y, 0);

        if (transform.position.x <= 0f)
        {
            EZ_PoolManager.Despawn(transform);
        }
    }
}
